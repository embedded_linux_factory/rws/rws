#!/bin/bash
#
# Setup your network configuration (in ELF docker context)
# Test SSH connection between the QEMU machine and your host

set -eu

################################## Helpers #####################################
usage()
{
    echo "Syntax: $(basename "${BASH_SOURCE[0]}") [-h | --help] [test]"
    echo "  options:"
    echo "      -h, --help    Print help"
    echo "  commands:"
    echo "      test    Test SSH connection"
}

help()
{
    echo "Setup your network configuration (in ELF docker context)"
    echo "Test SSH connection between the QEMU machine and your host"
    usage
}

############################## Global variables ###############################
SCRIPT_NAME="$(realpath "${BASH_SOURCE[0]}")"
readonly SCRIPT_NAME
MANIFEST="$(dirname "$SCRIPT_NAME")/.manifest.xml"
readonly MANIFEST

################################## Executing ##################################

# Get the options
while getopts ":h-:" options; do
    case $options in
        h)
            help
            exit 0;;

        # long options
        -)
            case $OPTARG in
                help)
                    help
                    exit 0;;
                *)
                    echo "Error syntax: --$OPTARG"
                    usage
                    exit 1;;
            esac;;
        \?)
            echo "Error syntax: -$OPTARG"
            usage
            exit 1;;
    esac
done

# Skip options already processed
shift $((OPTIND - 1))

# Commands processing
if [ $# -gt 0 ]; then
    case $1 in
        test)
            TEST_MODE="1";;
        *)
            echo "Unknown command $1"
            help
            exit 1;;
    esac
fi

if [[ ! -c "/dev/net/tun" ]]; then
    echo "Network setup is not possible, please run docker with TUN device : elfrun -i -t"
    exit 0
fi

NETWORK="$(/opt/elf/scripts/manifest.py --manifest="${MANIFEST}" docker --tag subnet --basic-output)"

if [[ -z $NETWORK ]]; then
    echo "Subnet selected in manifest is incorrect, please configure it in the \
manifest like this :   \"<docker subnet=\"10.0.0.0\"/>\""
    exit
fi

NETWORK_BASE=$(echo "$NETWORK" | cut -d'/' -f1 | cut -d'.' -f1-3)
MASK=$(echo "$NETWORK" | cut -d'/' -f2)

TAP_IP="${NETWORK_BASE}.1/${MASK}"

# Save username
USER="$(whoami)"

# TAP interface creation to run RWS image
sudo tunctl -t tap0 -u "$USER"

# Bring interfaces up
sudo ifconfig tap0 up

# Assign to tap0 an IP address
sudo ifconfig tap0 "$TAP_IP"

# Create a mask over subnet addresses to become as eth0
sudo iptables -t nat -A POSTROUTING -s "$NETWORK" -o eth0 -j MASQUERADE

echo "$SCRIPT_NAME: Network setup finished"

if [[ -n $TEST_MODE ]]; then

    # Build the new image if there is not, or if there was changes
    build.sh 

    # Emulate RWS image and wait before a SSH connection attempt
    ../../qemurun.sh &

    #TODO: maybe find another way to wait the complete launch before attempt
    sleep 100

    echo "
    $SCRIPT_NAME: Testing ssh communication with $QEMU_IP..."
    retcode="$(ssh -o "StrictHostKeyChecking=no" root@"$QEMU_IP" "ls; echo test " 2>/dev/null &)"

    if [ -n "$retcode" ] 
    then
        echo "$SCRIPT_NAME: SSH with QEMU success"
    else
        echo "$SCRIPT_NAME: SSH with QEMU fail"
        exit 1
    fi
else
    echo "  If you're in a container, check if there is a route to Qemu VM or add it (sudo ip route add $NETWORK via <docker_eth0_ip>)."
fi
