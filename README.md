# .manifest.xml

This is the manifest to build RWS with [elf](https://gitlab.com/embedded_linux_factory/elf/-/tree/main) (the rewrited one on the main branch, not master)

When you add a layer, as possible, try to specify your layer revision to be reproductible for the other users. Otherwise it will download the latest.

A layer revision file _revisions.txt_ can be used as necessary. It will be created and then updated when running _elfrun_.

# Getting started

First we need elf, so get it :
```bash
git clone -b main https://gitlab.com/embedded_linux_factory/elf.git
```
Then, we need a manifest, either by cloning this repo :
```bash
git clone -b scarthgap https://gitlab.com/embedded_linux_factory/rws/rws.git
cd rws
```
or by getting only the manifest :
```bash
mkdir rws && cd $_
wget https://gitlab.com/embedded_linux_factory/rws/rws/-/raw/scarthgap/.manifest.xml
```
It might be usefull to run this command :
```bash
sudo usermod -a -G docker $USER
```
Next we can launch elf :
```bash
../elf/elfrun
```
with `-c` you can choose the config, default is the first one in the manifest.

We are now in the container, let's build the image
```bash
build.sh
```
We can quit the container with
```bash
exit
```
The built image is in `build/rws-rpi3/tmp-glibc/deploy/images/`

# Running RWS Image in QEMU

This document provides instructions on how to run the RWS image in QEMU.

## Using ELF in docker mode

In this configuration, all installations are already set.  

The first and simplier way to emulate your image with QEMU is with the script :  
```bash
  qemurun.sh
```  
It refers to the `runqemu` command provided the Poky layer. For example : `runqemu qemux86-64 ext4 nographic ...`

At the end, the real qemu command looks like this :
```bash
qemu-system-x86_64 \
  -machine q35 \
  -device virtio-net-pci,netdev=net0,mac=52:54:00:12:34:02 \
  -netdev tap,id=net0,ifname=tap0,script=no,downscript=no \
  -object rng-random,filename=/dev/urandom,id=rng0 \
  -device virtio-rng-pci,rng=rng0 \
  -drive file=<path/to/your/image>.rootfs.ext4,if=virtio,format=raw \
  -cpu IvyBridge \
  -smp 1 \
  -m 4096 \
  -serial mon:vc \
  -serial null \
  -kernel <path/to/your/image>/bzImage \
  -append 'console=ttyS0,115200 root=/dev/vda \
  rw ip=<ip_addr>::<gateway>:<mask>::eth0:off:8.8.8.8 \
  net.ifnames=0 oprofile.timer=1 tsc=reliable no_timer_check rcupdate.rcu_expedited=1 swiotlb=0' \
  -vga none
```

The `qemurun.sh` script uses network configuration set in the manifest file.  
Make sure a TUN device is available by running `elfrun -i -t`.


## In your environment (without ELF)

- Ensure that QEMU is installed on your system. For this project, 
  QEMU version 6.2.0 is used.

- You need a prebuilt image and kernel. The relevant files are:
  - `core-image-minimal-qemux86-64.rootfs.ext4` or simply `<your_image>.rootfs.ext4`
  - `bzImage` (kernel image)

- **Previous steps should be availables but it strongly recommended to use ELF.**

 
## Networking

To allow networking between the image and the container, you have to setup your Docker in the correct way.
If you're using ELF, don't forget to mention the `-t` argument like this :

  ```bash
  <path_to_elf>/elf/elfrun -i -t
  ```

This will permit to setup a TAP device to communicate directly between the Qemu machine and your container.
Indeed, you can use the script to setup all this before running your image with Qemu.  
```bash
  network_setup.sh
```

Then you can run :  
```bash
  qemurun.sh
```
It will get your manifest configuration.

This, is runnning : `runqemu qemux86-64 ext4 nographic bootparams="ip=<ip_addr>::<gateway>:<mask>::eth0:off:8.8.8.8"`

For the following indications we've used thanks to the manifest : 
- _ip\_addr_ : 10.0.0.2
- _gateway_ : 10.0.0.1
- _mask_ : 255.255.255.0

At this step, you should be able to ping your docker interface thanks to created route (`10.0.0.0.1` in our example).  

In addition, after few configurations you will be able to communicate between your host (out of container) and the Qemu VM. If there isn't one, you'll need to create a route to forward all ip packets to 10.0.0.0/24, which is the subnet chosen by the docker through its ip address (e.g. _172.17.0.2_):

  ```bash
  sudo ip route add 10.0.0.0/24 via 172.17.0.2
  ```

Then you will be able to reach the Qemu VM from your host :
  
  ```bash
  ping 10.0.0.2
  ```


### Test SSH connection

It's possible to simply test the connection from the container with ELF like this :
  ```bash
  <path_to_elf>/elf/elfrun -t test
  ```
