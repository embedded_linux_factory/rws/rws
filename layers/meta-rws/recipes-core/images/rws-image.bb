SUMMARY = "RWS Custom Image with SSH support."
DESCRIPTION = "This is a custom image for the RWS project."
LICENSE = "MIT"

inherit core-image

IMAGE_INSTALL:append = " packagegroup-rws"

EXTRA_IMAGE_FEATURES += "ssh-server-openssh"

IMAGE_INSTALL:append = " webapp webapp-plugin-logs webapp-plugin-system-manager webapp-plugin-network webapp-plugin-users webapp-plugin-homepage webapp-plugin-example"

IMAGE_INSTALL += " iptables "

IMAGE_INSTALL += " gdb webapp-dbg webapp-src "

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE:append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "", d)}"
