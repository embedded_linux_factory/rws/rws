UBOOT_BOOTPART:odroid-xu4 ="1"
BOOT_PREFIX:odroid-xu4 = ""
UBOOT_LOAD_CMD:odroid-xu4 = "fatload"
# IMAGE_INSTALL:append:odroid-xu4 = "u-boot"
IMAGE_BOOT_FILES = "${UBOOT_SCRIPT} exynos5422-odroidxu4.dtb zImage"

DEVICE_DISK = "mmcblk1"

WKS_FILE = "rws.wks"
IMAGE_FSTYPES = "wic wic.bmap"
WIC_CREATE_EXTRA_ARGS = ""
WICVARS:append = " DEVICE_DISK"
