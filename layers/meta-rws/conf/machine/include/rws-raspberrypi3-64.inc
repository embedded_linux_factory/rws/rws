RPI_USE_U_BOOT:raspberrypi3-64 = "1"
ENABLE_UART:raspberrypi3-64 = "1"

DEVICE_DISK = "mmcblk0"

WKS_FILE = "rws.wks"
IMAGE_FSTYPES = "wic wic.bmap"
WIC_CREATE_EXTRA_ARGS = ""
WICVARS:append = " DEVICE_DISK"
