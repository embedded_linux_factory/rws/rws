SUMMARY = "Library dependency for DUT Manager"
DESCRIPTION = "Controls usb relays using hid Linux API"
SECTION = "rws"
LICENSE = "GPL-2.0"

S = "${WORKDIR}/git"

LIC_FILES_CHKSUM = "file://LICENSE.md;md5=e8c1458438ead3c34974bc0be3a03ed6"

SRC_URI = "git://github.com/darrylb123/usbrelay;protocol=https;branch=master \
           https://gitlab.com/embedded_linux_factory/rws/dut-manager/-/raw/main/patches/001-usbrelay_rename_shutdown_func.patch;name=patch_dut_001 \
           file://001-usbrelay_change_group.patch;name=patch_001 \
           "
SRCREV = "b3fd5b6e825de0c5efdab4d511088d621fbdb649"

SRC_URI[patch_dut_001.md5sum] = "d7fc7b801f4bee58fc29e8e3ae292c57"
SRC_URI[patch_001.md5sum] = "a86abdeb54e5481eb5d4682c3a26a7c9"

DEPENDS = "hidapi ldconfig-native"
RDEPENDS:${PN} = "hidapi"

inherit lib_package pkgconfig

FILES:${PN} += "/etc/udev/rules.d/50-usbrelay.rules"

EXTRA_OEMAKE += "PREFIX=${prefix} LIBDIR=${libdir}"

do_install() {
    LDCONFIG=${STAGING_BINDIR_NATIVE}/ldconfig DESTDIR=${D} oe_runmake install

    install -d 0755 ${D}/etc/udev/rules.d
    install -m 0644 ${S}/50-usbrelay.rules ${D}/etc/udev/rules.d/50-usbrelay.rules
}
