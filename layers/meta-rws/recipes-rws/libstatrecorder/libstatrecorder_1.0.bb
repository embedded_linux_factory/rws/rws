SUMMARY = "Stats Recorder Library"
DESCRIPTION = "Record CPU stats"
SECTION = "rws"
LICENSE = "GPL-2.0-only"

LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=b9657b1ee23f2bf9f457b8a756b82b30"

SRC_URI = "git://gitlab.com/embedded_linux_factory/libstatrecorder.git;protocol=https;branch=main"
SRCREV = "6d85b90dc56993941bcc9ece2c44b3d447580556"

S = "${WORKDIR}/git"

DEPENDS += "libnl"

RDEPENDS_${PN} = "libnl"

inherit meson pkgconfig
