#!/bin/bash
#
# Run Qemu simply with manifest information

set -eu

################################## Helpers #####################################
usage()
{
    echo "Syntax: $(basename "${BASH_SOURCE[0]}") [-h | --help]"
    echo "  options:"
    echo "      -h, --help    Print help"
}

help()
{
    echo "Run Qemu simply with manifest information."
    usage
}

############################## Global variables ###############################
SCRIPT_NAME="$(realpath "${BASH_SOURCE[0]}")"
readonly SCRIPT_NAME
MANIFEST="$(dirname "$SCRIPT_NAME")/.manifest.xml"
readonly MANIFEST

################################## Executing ##################################

# Get the options
while getopts ":h-:" options; do
    case $options in
        h)
            help
            exit 0;;

        # long options
        -)
            case $OPTARG in
                help)
                    help
                    exit 0;;
                *)
                    echo "Error syntax: --$OPTARG"
                    usage
                    exit 1;;
            esac;;
        \?)
            echo "Error syntax: -$OPTARG"
            usage
            exit 1;;
    esac
done

# Checking if a TUN device is available
if [[ -c "/dev/net/tun" ]]; then

    # Checking if a TAP device has been created
    if [[ $( ! ip addr | grep -q "tap0") ]]; then
        echo "$SCRIPT_NAME: WARNING Network isn't completelly set, if you want it \
run $(dirname "$SCRIPT_NAME")/network_setup.sh"

        # To let to the user the time to see this information 
        sleep 2
    fi

    NETWORK="$(/opt/elf/scripts/manifest.py \
            --manifest="${MANIFEST}" docker --tag subnet --basic-output)"
    if [[ -z $NETWORK ]]; then
        echo "Subnet selected in manifest is incorrect, please configure it in the \
manifest like this :   \"<docker subnet=\"10.0.0.0\"/>\""
        exit
    fi

    NETWORK_BASE=$(echo "$NETWORK" | cut -d'/' -f1 | cut -d'.' -f1-3)
    QEMU_IP="${NETWORK_BASE}.2"

    runqemu qemux86-64 ext4 nographic \
            bootparams="ip=$QEMU_IP::$NETWORK_BASE.1:255.255.255.0::eth0:off:8.8.8.8"
else
    echo "$SCRIPT_NAME: WARNING TUN device doesn't set, running Qemu in slirp \
mode"
    echo "  If you want to use your network configuration, \
please run elfrun -i -t"

    # To let to the user the time to see this information 
    sleep 2
    runqemu qemux86-64 ext4 nographic slirp
fi

